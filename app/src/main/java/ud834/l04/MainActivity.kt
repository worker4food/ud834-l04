package ud834.l04

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        address.setOnClickListener {
            val url = Uri.parse("google.streetview:layer=c&panoid=SefiE5bjDn074M5LefknHg&cbp=1,145,,0.3,3.9")

            Intent(Intent.ACTION_VIEW, url)
                .apply { setPackage("com.google.android.apps.maps") }
                .also(::startActivity)
        }
    }
}
